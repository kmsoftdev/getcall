package es.esy.kmsoftwaredev.getcall;

import android.util.Log;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetCall {

    public void TestGetCall() {
        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        HttpUrl url = new HttpUrl.Builder()

                .scheme("https")
                .host("mobile.twitter.com")
                .addPathSegment("search")
                .addQueryParameter("q", "nbadraft")
                .build();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.d("kmtag", "response: " + response.body().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
